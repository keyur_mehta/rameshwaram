var pushNotification;

var rameshwaramApp=angular.module('rameshwaramApp', [ "ngRoute","ngStorage","mobile-angular-ui","ui.bootstrap"]);
/* to display special characters*/
rameshwaramApp.filter('html',function($sce){
    return function(input){
        return $sce.trustAsHtml(input);
    }
})
function openlinks(url)
{
	window.open(url, "_system");
}
function exapp()
{
	navigator.app.exitApp();
}
function onBackKeyDown()
 {
    navigator.app.exitApp();  
}
function removeHTMLTags(){
		var strInputCode = document.getElementById("overviewid").innerHTML;
		strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1){
			return (p1 == "lt")? "<" : ">";
		});
		var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
		return strTagStrippedText;
}


rameshwaramApp.config(function($routeProvider) {
        $routeProvider
            // route for the home page
            .when('/home', {
                templateUrl : 'partials/home.html',
                controller  : 'mainController',
                label: 'Home'
            })
            .when('/success-story', {
                templateUrl : 'partials/success-story.html',
                controller  : 'success-storyController'
            })
            .when('/news', {
                templateUrl : 'partials/news.html',
                controller  : 'newsController'
            })
            .when('/contact-us', {
                templateUrl : 'partials/contact-us.html',
                controller  : 'contactController',
                label: 'Contact Us'
            })
            .when('/about', {
                templateUrl : 'partials/about.html',
                controller  : 'aboutController'                  
            })
            .when('/subscribe', {
                templateUrl : 'partials/subscribe.html',
                controller  : 'subscribeController',
            })
            .when('/ProjectSelect/', {
                templateUrl : 'partials/allprojects.html',
                controller  : 'allprojectController',
                label: 'Projects'
            })
            .when('/Project/:projectId', {
                templateUrl : 'partials/project-detail.html',
                controller  : 'projectDetailController',
                label: 'Projects'
            })
            .when('/Project-Option', {
                templateUrl : 'partials/project-options.html',
                controller  : 'projectoptionController',
                label: 'Projects'
            })
            .when('/Search-Project/:typeID/:status', {
                templateUrl : 'partials/search-project.html',
                controller  : 'searchProjectController',
                label: 'Projects'
            })
            .when('/gallery', {
                templateUrl : 'partials/gallery.html',
                controller  : 'galleryController',
                label: 'Gallery'
            })
            .when('/reach-us', {
                templateUrl : 'partials/reachus.html',
                controller  : 'reachusController',
                label: 'Reach Us'
            })
            .otherwise({
            	  templateUrl : 'partials/subscribe.html',
                  controller  : 'subscribeController',
      	   });           
});

/* called on every route*/
rameshwaramApp.run(function ($rootScope,$localStorage,$http,$location) {
   /* set title for all pages */ 
  $rootScope.title = function(title) 
   {
 		document.getElementById('brand-title').innerHTML =title;
   };
   
   /* set header for all pages */ 	
   $rootScope.header = function(common) 
   {
 		$rootScope.defaultheader = common;
		
   };

    /* set footer for all pages */ 
   $rootScope.footer = function(common) 
   {
 		$rootScope.innerfooter = common;
		
   };
   
    $rootScope.$storage = $localStorage.$default({
          subscribe : 0
    });  
	
     /* check for internet connection*/
     $rootScope.checkConnection = function() 
     {
 	/* if not connected return true*/
	if(navigator.network.connection.type == Connection.NONE)
	{	
		navigator.notification.alert('Please check your Internet Connection', function(){}, "Warning", "");	
    		$location.path('/home');
	}
	else
	{
		
	}
						
     };  
    /* end of connection check */

     /* downloading data by open file native plugin*/
     $rootScope.downloaddata = function(url) 
     {
 	window.openFileNative.open(url); 	
     };
     
     /* open photogallery*/ 
     $rootScope.opengallery = function(imgid) 
 	{
 		var fpath = 'http://www.rameshwaramgroup.com/photogallery.php?imgid=' + imgid;
 		
 		var ref = window.open(fpath, '_blank', 'location=no');	
 		ref.addEventListener('loadstart', function(event) 
 		{ navigator.notification.activityStart("Loading", "Please wait for a while."); }
 		);
         	ref.addEventListener('loadstop', function(event) { navigator.notification.activityStop(); 
		
         	});
     };
   /* end of photogallery */
	
     /* open floorplangallery*/ 
     $rootScope.floorplangallery = function(projectid,planid) 
 	{
 		var fpath = 'http://www.rameshwaramgroup.com/floorplangallery.php?projectid=' + projectid + '&floorid=' + planid;
 		var ref = window.open(fpath, '_blank', 'location=no');	
 		ref.addEventListener('loadstart', function(event) 
 		{ navigator.notification.activityStart("Loading", "Please wait for a while."); }
 		);
         	ref.addEventListener('loadstop', function(event) { navigator.notification.activityStop(); 
		         	
         	});
     };
    /* end of floorplan gallery*/
    
    /* open projectgallery*/  
     $rootScope.projectgallery = function(projectid) 
     {
 		var fpath = 'http://www.rameshwaramgroup.com/projectgallery.php?projectid=' + projectid;
 		var ref = window.open(fpath, '_blank', 'location=no');	
 		ref.addEventListener('loadstart', function(event) 
 		{ navigator.notification.activityStart("Loading", "Please wait for a while."); }
 		);
         	ref.addEventListener('loadstop', function(event) { 
         	navigator.notification.activityStop();     	
         	 }); 
     };
    /* end of projectgallery*/
     $rootScope.backfun = function() {
    		$location.path('/home');
      }
      $rootScope.subtitle = function(title) 
   	{
 		document.getElementById('sub-title').innerHTML =title;
   	};
});

/* serailiaze data for sending to server */
function serialize(obj, prefix) {
    var str = [];
    for (var p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
              v = obj[p];
          str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}

rameshwaramApp.controller('mainController', function($scope,$http,$rootScope,$location,$window) {

	$scope.latest = null;
	$rootScope.title('Home');
	$rootScope.header(true);
	$rootScope.footer(true);
	$scope.imgarray = ["img/slider1.jpg","img/slider2.jpg","img/slider3.jpg","img/slider4.jpg"];
 	$scope.myInterval = 3000;
	
 	document.addEventListener("backbutton", onBackKeyDown, false);

});

rameshwaramApp.controller('aboutController', function($scope,$http,$window,$rootScope,$location) {	
		$rootScope.checkConnection();
    		$rootScope.title('About Us');
		$rootScope.header(true);
		$rootScope.footer(true);
    		$scope.about = null;
    		$scope.loader = true;
    		$scope.$watch('loader', function (newvalue, oldvalue, $scope) {	
  		     $scope.loader = newvalue;
  		});
  		 
  		$http.get('http://www.rameshwaramgroup.com/data/about.php')
         	.success(function (data) {
            	 $scope.about = data;
            	 $scope.items = [
                    {
                        name: "item1",
                        desc: "Company Profile",
                        information : $scope.about.profile
                    },
                    {
                        name: "item2",
                        desc: "Our Values",
                        information : $scope.about.ourvalue 
                    },
                    {
                        name: "item3",
                        desc: "Flagship Companies",
                        information : $scope.about.flagship
                    },
                    {
                        name: "item4",
                        desc: "Our Team",
                        information : $scope.about.team
                    },
                     {
                        name: "item5",
                        desc: "Future Plans",
                        information : $scope.about.futureplan
                    }
                ];
			$scope.default = $scope.items[1];
			$scope.$parent.isopen = ($scope.$parent.default === $scope.item);
			$scope.loader = false;
         		$scope.$watch('isopen', function (newvalue, oldvalue, $scope) {
                   		 $scope.$parent.isopen = newvalue;
         		});
       		 })
         	.error(function (data, status, headers, config) {
         	});
         	
    	
      document.removeEventListener("backbutton", onBackKeyDown, false);
});

rameshwaramApp.controller('success-storyController', function($scope,$rootScope,$http,$window,$location) {

			$rootScope.checkConnection();
			$rootScope.title('Success Stories');
			$rootScope.header(true);
			$rootScope.footer(true);
			$scope.storyList = null;
			$scope.loader = true;
			$scope.$watch('loader', function (newvalue, oldvalue, $scope) {	
  		     		$scope.loader = newvalue;
  			});
  		 
			$http.get('http://www.rameshwaramgroup.com/data/success-story.php')
         			.success(function (data) {
            	 		$scope.storyList = data;
            	 		$scope.loader = false;
        		 })
        		 .error(function (data, status, headers, config) {   
        		 });
          	
      	document.removeEventListener("backbutton", onBackKeyDown, false);
});

rameshwaramApp.controller('newsController', function($scope,$rootScope,$http,$window,$location) {
		$rootScope.checkConnection();
		
		$rootScope.title('News & Events');
		$rootScope.header(true);
		$rootScope.footer(true);
		$scope.newsList = null;
		$scope.loader = true;
		$scope.$watch('loader', function (newvalue, oldvalue, $scope) {	
  		     $scope.loader = newvalue;
  		});
		$http.get('http://www.rameshwaramgroup.com/data/news.php')
         		.success(function (data) {
             		$scope.newsList = data;
             		$scope.loader = false;
     		 })
      		.error(function (data, status, headers, config) {
       		});
      document.removeEventListener("backbutton", onBackKeyDown, false);
});
rameshwaramApp.controller('galleryController', function($scope,$http,$window,$rootScope,$location) {
	$rootScope.checkConnection();
	$rootScope.title('Gallery');
	$rootScope.header(true);
	$rootScope.footer(true);
  	$scope.loader = true;
	setTimeout(function() {
    			$scope.$apply(function() {
     			$scope.loader = false;
    			});
  			}, 2000);	
  	$scope.imgappend = '';
    	if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile1.jpg";
    	}else {
    		$scope.imgappend = "";
    	}
	
    	$scope.galleryList = null;
	$http.get('http://www.rameshwaramgroup.com/data/gallery.php')
     	.success(function (data) {
     	$scope.galleryList = data;
     	}).error(function (data, status, headers, config) {}); 		
	document.removeEventListener("backbutton", onBackKeyDown, false);
});

rameshwaramApp.controller('contactController', function($scope,$http,$route,$window,$rootScope,$location) {
	$rootScope.title('Inquiry');
	$rootScope.header(true);
	$rootScope.footer(true);
	$scope.loader = true;
	$rootScope.checkConnection();
	setTimeout(function() {
    	$scope.$apply(function() {
     		$scope.loader = false;
    	});
  	}, 2000);	
  	
  	$scope.formInfo = {};
    	$scope.nameerror = false;
    	$scope.contacterror = false;
    	$scope.subjecterror = false;
    	$scope.messageerror = false;
    	$scope.emailerror = false;
    	$scope.errorfun= function (id) 
    	{
    		if(id == 1)	  { $scope.nameerror = true; }
    		else if(id == 2) { $scope.emailerror = true;  }
    		else if(id == 3) { $scope.contacterror = true; }
    		else if(id == 4) { $scope.subjecterror = true; }    
    		else if(id == 5) { $scope.messageerror = true; }    			
		else { }	
	};    
	$scope.formInfo.submitTheForm = function(item, event) {
       
     	var dataObject = {
          name : $scope.formInfo.name,
          email : $scope.formInfo.email,
          contact : $scope.formInfo.contact,
          subject : $scope.formInfo.subject,
          message : $scope.formInfo.message
     };
     
     //serialize data before sending
    var serdata = serialize(dataObject);
	$http({
    	  url: "http://www.rameshwaramgroup.com/data/sendcontact.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	}).success(function(data, status, headers, config) {
    		
    		if(data.msg == '1')
    		{
    			//navigator.notification.alert('Your inquiry has been send successfully...We will be in touch with you Soon...', function(){}, "Success", "");
    		}
    		else
    		{
    			
    		}

		$route.reload();   	
    	}).error(function(data, status, headers, config) {	
    			//navigator.notification.alert('Submitting form failed!', function(){}, "Failure", "");	
	});	   
     }		
	
	document.removeEventListener("backbutton", onBackKeyDown, false);
});
rameshwaramApp.controller('subscribeController', function($scope,$http,$route,$window,$rootScope,$location) {
	
	
	if($rootScope.$storage.subscribe == 1)
	{
		$location.path('/home');
		
	}
	
		
	$rootScope.header(false);
	$rootScope.footer(false);
	$rootScope.subtitle('Subscribe');
	
	if($rootScope.$storage.subscribe == 0){
	$scope.loader = true;
	setTimeout(function() {
    	$scope.$apply(function() {
     			$scope.loader = false;
    			});
  	}, 2000);		
  	}
	$scope.formInfo = {};
    	$scope.nameerror = false;
    	$scope.contacterror = false;
    	$scope.emailerror = false;
    	$scope.errorfun= function (id) 
    	{
    		if(id == 1)	  { $scope.nameerror = true; }
    		else if(id == 2) { $scope.emailerror = true;  }
    		else if(id == 3) { $scope.contacterror = true; }		
		else { }	
	}; 
	$scope.formInfo.submitTheForm = function(item, event) {
      
     	/* if not connected return true*/
	if(navigator.network.connection.type == Connection.NONE)
	{	
		navigator.notification.alert('Please check your Internet Connection', function(){}, "Warning", "");	
    		$location.path('/subscribe');
	}
	
       var dataObject = {
          subname : $scope.formInfo.name,
          subemail : $scope.formInfo.email,
          submobile : $scope.formInfo.mobile
       };
      // serialize data before sending       
      var serdata = serialize(dataObject);
	  $http({
    	 url: "http://www.rameshwaramgroup.com/data/subscription.php",
         method: "POST",
         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	}).success(function(data, status, headers, config) {
		if(data.msg == 1)
    		{
    			navigator.notification.alert('Your subscription has been done successfully...', function(){}, "Success", "");	
    		}
    		else
    		{
    			navigator.notification.alert('You are already subscribed to this application...', function(){}, "Already Applied", "");	
    		}
    		 $rootScope.$storage.subscribe = 1;
		 $location.path("/home") ;
		
    		}).error(function(data, status, headers, config) {
    			navigator.notification.alert('There occured some problem submitting the form..Please Try Again', function(){}, "", "");
		});	
		 	
     }
 
});
rameshwaramApp.controller('allprojectController', function($scope,$http,$window,$rootScope,$location) {	
		$rootScope.title('All Projects');
		$rootScope.header(true);
		$rootScope.footer(true);
		$rootScope.checkConnection();
    		$scope.loader = true;
    		$scope.selectedstatus = '';
		$scope.selectedcat = 'Residential';
		
			
    		$scope.imgappend = '';
    		if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile.jpg";
    		}else {
    		$scope.imgappend = "";
    		}
    		
  			 setTimeout(function() {
    			$scope.$apply(function() {
     			$scope.loader = false;
    			});
  			}, 2000);	
  	  	
    		$scope.projectList = null;
    		$http.get('http://www.rameshwaramgroup.com/data/projectlist.php')
     		.success(function (data) {
     			$scope.projectList = data;
     			
     		}).error(function (data, status, headers, config) {}); 
     		
     		$scope.changetype = function(type) 
     		{	
				if(type == 'All')
				{
					$scope.selectedstatus = '';
				}
				else
				{
					$scope.selectedstatus =  type;
				}
				$rootScope.title(type + ' Projects');
				$scope.status.isopen = !$scope.status.isopen;		
      		};
		$scope.changecat = function(cat) 
     		{	
				
			$scope.selectedcat = cat;		
      		};	
     
      	document.removeEventListener("backbutton", onBackKeyDown, false);		
});

rameshwaramApp.controller('projectDetailController', function($scope,$http,$routeParams,$rootScope,$location) {
	 $rootScope.checkConnection();	
	 $rootScope.header(true);
	 $rootScope.footer(true);
    	 $scope.projectDetail = null;
    	 $scope.projectId = $routeParams.projectId;
    	 $scope.display = true;
    	
    		$scope.loader 		= true;
		$scope.doverview 	= true;
		$scope.damenities	= false;
		$scope.dfloorplan 	= false;
		$scope.dlocation 	= false;
		$scope.dbrochure 	= false;
		$scope.dshare		= false;
		$scope.imgappend = '';
   	if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile.jpg";
   	 }else {
    		$scope.imgappend = "";
    	}
   
     	setTimeout(function() {
    			$scope.$apply(function() {
     			$scope.loader = false;
    			});
  		}, 2000);		
		
		var dataObject = {
          	projectid : $scope.projectId 
       	};
      // serialize data before sending       
      	var serdata = serialize(dataObject);
		$http({
    	  url: "http://www.rameshwaramgroup.com/data/projectdetail.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	   }).success(function(data, status, headers, config) {
      	$scope.projectDetail = data;
	$rootScope.title($scope.projectDetail.title);

      	
      	$scope.floorarray = $scope.projectDetail.floorplan.split(',');
      	$scope.floortitlearray = $scope.projectDetail.floorplantitle.split(',');
      	
      	if($scope.floorarray == '')
      	{
      		$scope.display = false;
      	}
      
    		}).error(function(data, status, headers, config) {
     	
		});
		$scope.downloadbrochure = function() 
     		{
			navigator.notification.confirm(
            			'Do You want to download the brochure?',  
            			function(button)
				{  if(button == 1){ window.openFileNative.open($scope.projectDetail.brochure); }
       				},        
            			'Brochure Download',            
            			'Yes,No'        
       				 );		
		}
		$scope.sharefun = function() 
     		{
			var overviewtxt= removeHTMLTags();
			var imgsrc = $scope.projectDetail.gallery + $scope.imgappend ; 
			window.plugins.socialsharing.share(overviewtxt,$scope.projectDetail.title, imgsrc, $scope.projectDetail.weburl);
		}
		
		
  	
      document.removeEventListener("backbutton", onBackKeyDown, false);	
});

rameshwaramApp.controller('projectoptionController', function($scope,$http,$location,$rootScope,$location) {
			
  			$scope.loader = true;
			$rootScope.header(true);
			$rootScope.footer(true);
			$scope.status = 'On Going';	  
			$rootScope.title('Search Projects');	
			$rootScope.checkConnection();
			
    			 setTimeout(function() {
    			$scope.$apply(function() {
     			$scope.loader = false;
    			});
  			}, 2000);	   
	     
     			$scope.type = '';
     			$http({
    	  		url: "http://www.rameshwaramgroup.com/data/producttype.php",
        		method: "POST",
        		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    			}).success(function(data, status, headers, config) {
      		$scope.projectType = data;
      		$scope.type = $scope.projectType[0];
      		 $scope.projectt = $scope.projectType[0].typeid;
      		
    			}).error(function(data, status, headers, config) {
     			
				});	 
     			
        		/* start of function on change */ 	
        
       	 $scope.getProjects = function(typeID) {
				 var dataObject = {
        			typeid : typeID
     			};
     			
     			var serdata = serialize(dataObject);
     			$http({
    	  		url: "http://www.hindva.com/data/producttype.php",
        		method: "POST",
        		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        		data: serdata
    			}).success(function(data, status, headers, config) {
      			$scope.projectType = data;
      			$scope.type = $scope.projectType[0];
      			$scope.projectt = $scope.projectType[0].typeid;
    			}).error(function(data, status, headers, config) {
     				
				});	 
       };
       
        $scope.submitSearch = function() {
				var selectedtype= $scope.projectt ;
				var selectedstatus= $scope.status;
				$location.path('/Search-Project/'+selectedtype + '/' + selectedstatus,false);
       };
     
      document.removeEventListener("backbutton", onBackKeyDown, false);
       
       /* end of function*/
         	
});
rameshwaramApp.controller('searchProjectController', function($scope,$http,$routeParams,$window,$rootScope,$location) {
	//$rootScope.title('Projects');	
	
	$scope.loader = true;
	$rootScope.header(true);
	$rootScope.footer(true);
	$rootScope.checkConnection();
	$scope.imgappend = '';
   	if(window.innerWidth <= 480){
    		$scope.imgappend = "_mobile.jpg";
    	}else {
    		$scope.imgappend = "";
    	}
	
     setTimeout(function() {
    	$scope.$apply(function() {
     			$scope.loader = false;
    			});
  	}, 2000);	
  	 
    	$scope.projectList = null;
    	$scope.type = $routeParams.typeID;
    	$scope.status = $routeParams.status;
    	$scope.titlestatus = $routeParams.status;
    	$scope.detail = null;
	
    	if($scope.status == 'All')
    	{
    		$scope.status = '';
    	}
    	
		  var dataObject = {
          typeid : $scope.type
       };
      // serialize data before sending       
      var serdata = serialize(dataObject);
     
	  $http({
    	  url: "http://www.rameshwaramgroup.com/data/searchproject.php",
        method: "POST",
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: serdata
    	}).success(function(data, status, headers, config) {
      		$scope.projectList = data;
      		
    	}).error(function(data, status, headers, config) {
     		
		});
		
		 var dataObject1 = {
          project : $scope.type
       };
	var serdata1 = serialize(dataObject1);
	$http({
    	  		url: "http://www.rameshwaramgroup.com/data/producttype.php",
        		method: "POST",
        		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        		data: serdata1
    			}).success(function(data, status, headers, config) {
    				$scope.detail =data;
				$rootScope.title($scope.detail[0].typetitle);	
    				
    			}).error(function(data, status, headers, config) {
     		
				});	
		  
      	document.removeEventListener("backbutton", onBackKeyDown, false);		
});
rameshwaramApp.controller('reachusController', function($scope,$rootScope,$location) {
	$rootScope.header(true);
	$rootScope.footer(true);
	$scope.info 		= true;
	$scope.socialmedia 	= false;
	$scope.ratecontent 	= false;
	$rootScope.title('Reach Us');	
	$rootScope.checkConnection();
	
	$scope.changeview = function(selectedtab) {       
     			if(selectedtab == 'info')
     			{
     				$scope.info 		= true;
				$scope.socialmedia 	= false;
				$scope.ratecontent 	= false;
			
     			}
			else if(selectedtab == 'contact')  
			{
				$scope.info 		= false;
				$scope.socialmedia 	= false;
				$scope.ratecontent 	= false;
			} 	
			else if(selectedtab == 'socialmedia')  
			{
				$scope.info 		= false;
				$scope.socialmedia 	= true;
				$scope.ratecontent 	= false;
			}  
			else if(selectedtab == 'ratecontent')  
			{
				$scope.info 		= false;
				$scope.socialmedia 	= false;
				$scope.ratecontent 	= true;
			}
	}    

  	document.removeEventListener("backbutton", onBackKeyDown, false);	 	  
});

